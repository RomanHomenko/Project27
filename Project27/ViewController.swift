//
//  ViewController.swift
//  Project27
//
//  Created by Роман Хоменко on 06.07.2022.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var currentDrawType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawRectangle()
    }

    @IBAction func reDrawButton(_ sender: UIButton) {
        currentDrawType += 1
        if currentDrawType > 7 {
            currentDrawType = 0
        }
        
        switch currentDrawType {
        case 0:
            drawRectangle()
        case 1:
            drawCircle()
        case 2:
            drawCheckerboard()
        case 3:
            drawRotatedSquares()
        case 4:
            drawLines()
        case 5:
            drawImagesAndText()
        case 6:
            drawEmojiSmile()
        case 7:
            drawTextTWI()
        default:
            break
        }
    }
    
}

// MARK: - Draw methods
extension ViewController {
    func drawRectangle() {
        let render = UIGraphicsImageRenderer(size: CGSize(width: 512,
                                                          height: 512))
        
        let image = render.image { ctx in
            let rectangle = CGRect(x: 0,
                                   y: 0,
                                   width: 512,
                                   height: 512)
            
            ctx.cgContext.setFillColor(UIColor.red.cgColor)
            ctx.cgContext.setStrokeColor(UIColor.black.cgColor)
            ctx.cgContext.setLineWidth(10)
            
            ctx.cgContext.addRect(rectangle)
            ctx.cgContext.drawPath(using: .fillStroke)
        }
        
        imageView.image = image
    }
    
    func drawCircle() {
        let render = UIGraphicsImageRenderer(size: CGSize(width: 512,
                                                          height: 512))
        
        let image = render.image { ctx in
            let rectangle = CGRect(x: 0,
                                   y: 0,
                                   width: 512,
                                   height: 512).insetBy(dx: 20,
                                                        dy: 20)
            
            ctx.cgContext.setFillColor(UIColor.red.cgColor)
            ctx.cgContext.setStrokeColor(UIColor.black.cgColor)
            ctx.cgContext.setLineWidth(10)
            
            ctx.cgContext.addEllipse(in: rectangle)
            ctx.cgContext.drawPath(using: .fillStroke)
        }
        
        imageView.image = image
    }
    
    func drawCheckerboard() {
        let render = UIGraphicsImageRenderer(size: CGSize(width: 512,
                                                          height: 512))
        
        let image = render.image { ctx in
            ctx.cgContext.setFillColor(UIColor.black.cgColor)
            
            for row in 0..<8 {
                for col in 0..<8 {
                    if (row + col) % 2 == 0 {
                        ctx.cgContext.fill(CGRect(x: col * 64,
                                                  y: row * 64,
                                                  width: 64,
                                                  height: 64))
                    }
                }
            }
        }
        
        imageView.image = image
    }
    
    func drawRotatedSquares() {
        let render = UIGraphicsImageRenderer(size: CGSize(width: 512,
                                                          height: 512))
        
        let image = render.image { ctx in
            ctx.cgContext.translateBy(x: 256,
                                      y: 256)
            
            let rotations = 16
            let amount = Double.pi / Double(rotations)
            
            for _ in 0..<rotations {
                ctx.cgContext.rotate(by: CGFloat(amount))
                ctx.cgContext.addRect(CGRect(x: -128,
                                             y: -128,
                                             width: 256,
                                             height: 256))
            }
            
            ctx.cgContext.setStrokeColor(UIColor.black.cgColor)
            ctx.cgContext.strokePath()
        }
        
        imageView.image = image
    }
    
    func drawLines() {
        let render = UIGraphicsImageRenderer(size: CGSize(width: 512,
                                                          height: 512))
        
        let image = render.image { ctx in
            ctx.cgContext.translateBy(x: 256,
                                      y: 256)
            
            var first = true
            var lenght: CGFloat = 256
            
            for _ in 0..<256 {
                ctx.cgContext.rotate(by: .pi / 2)
                
                if first {
                    ctx.cgContext.move(to: CGPoint(x: lenght,
                                                   y: 50))
                    first = false
                } else {
                    ctx.cgContext.addLine(to: CGPoint(x: lenght,
                                                      y: 50))
                }
                
                lenght *= 0.99
            }
            
            ctx.cgContext.setStrokeColor(UIColor.black.cgColor)
            ctx.cgContext.strokePath()
        }
        
        imageView.image = image
    }
    
    func drawImagesAndText() {
        let render = UIGraphicsImageRenderer(size: CGSize(width: 512,
                                                          height: 512))
        
        let image = render.image { ctx in
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            let attrs: [NSAttributedString.Key: Any] = [
                .font: UIFont.systemFont(ofSize: 36),
                .paragraphStyle: paragraphStyle,
            ]
            
            let string = "The best-lain schemes o\nmice an' men gang aft agley"
            
            let attributedString = NSAttributedString(string: string,
                                                      attributes: attrs)
            attributedString.draw(with: CGRect(x: 32,
                                               y: 32,
                                               width: 448,
                                               height: 448),
                                  options: .usesLineFragmentOrigin,
                                  context: nil)
            
            let mouse = UIImage(named: "mouse")
            mouse?.draw(at: CGPoint(x: 300,
                                    y: 150))
        }
        
        imageView.image = image
    }
    
    func drawEmojiSmile() {
        let render = UIGraphicsImageRenderer(size: CGSize(width: 512,
                                                          height: 512))
        
        let image = render.image { ctx in
            let rectangle = CGRect(x: 64,
                                   y: 64,
                                   width: 384,
                                   height: 384)
            
            let leftEye = CGRect(x: 192,
                             y: 192,
                             width: 10,
                             height: 10)
            let rightEye = CGRect(x: 320,
                             y: 192,
                             width: 10,
                             height: 10)
            
            ctx.cgContext.setFillColor(UIColor.yellow.cgColor)
            ctx.cgContext.setStrokeColor(UIColor.black.cgColor)
            ctx.cgContext.setLineWidth(10)
            ctx.cgContext.strokePath()
            
            ctx.cgContext.addEllipse(in: rectangle)
            ctx.cgContext.addEllipse(in: leftEye)
            ctx.cgContext.addEllipse(in: rightEye)
            ctx.cgContext.drawPath(using: .fillStroke)
            
            // smile properties
            ctx.cgContext.move(to: CGPoint(x: 192,
                                           y: 384))
            ctx.cgContext.addLine(to: CGPoint(x: 256,
                                              y: 400))
            ctx.cgContext.move(to: CGPoint(x: 256,
                                           y: 400))
            ctx.cgContext.addLine(to: CGPoint(x: 320,
                                              y: 384))
            ctx.cgContext.strokePath()
        }
        
        imageView.image = image
    }
    
    func drawTextTWI() {
        let render = UIGraphicsImageRenderer(size: CGSize(width: 512,
                                                          height: 512))
        
        let image = render.image { ctx in
            ctx.cgContext.setStrokeColor(UIColor.black.cgColor)
            ctx.cgContext.setLineWidth(10)
            
            // character T
            ctx.cgContext.move(to: CGPoint(x: 64,
                                           y: 128))
            ctx.cgContext.addLine(to: CGPoint(x: 128,
                                              y: 128))
            ctx.cgContext.move(to: CGPoint(x: 96,
                                           y: 128))
            ctx.cgContext.addLine(to: CGPoint(x: 96,
                                              y: 256))
            
            // character W
            ctx.cgContext.move(to: CGPoint(x: 128 + spacer,
                                           y: 128))
            ctx.cgContext.addLine(to: CGPoint(x: 144 + spacer,
                                              y: 256))
            ctx.cgContext.move(to: CGPoint(x: 144 + spacer,
                                           y: 256))
            ctx.cgContext.addLine(to: CGPoint(x: 160 + spacer,
                                              y: 128))
            
            ctx.cgContext.move(to: CGPoint(x: 160 + spacer,
                                           y: 128))
            ctx.cgContext.addLine(to: CGPoint(x: 176 + spacer,
                                              y: 256))
            ctx.cgContext.move(to: CGPoint(x: 176 + spacer,
                                           y: 256))
            ctx.cgContext.addLine(to: CGPoint(x: 192 + spacer,
                                              y: 128))
            
            // character I
            ctx.cgContext.move(to: CGPoint(x: 212 + spacer,
                                           y: 128))
            ctx.cgContext.addLine(to: CGPoint(x: 212 + spacer,
                                              y: 256))
            
            
            ctx.cgContext.strokePath()
        }
        
        imageView.image = image
    }
}
